/** General parameters - independent of geometry or PDE */

//Timestepping
DefineConstant[ t_0 = {0., Name "Timestepping/Start time"} ];
DefineConstant[ t_end = {1, Name "Timestepping/Stop time"} ];
DefineConstant[ t_step = {1e-1, Name "Timestepping/Step size"} ];
DefineConstant[ t_theta = {1., Name "Timestepping/Theta"} ];
