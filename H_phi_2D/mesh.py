import gmsh
import sys

gmsh.initialize()
gmsh.parser.parse("geometry_parameters.pro")


pt1 = gmsh.model.occ.addPoint(-0.7, -0.6, 0)
pt2 = gmsh.model.occ.addPoint(-0.3, -0.4, 0)
pt3 = gmsh.model.occ.addPoint(0.2, 0.7, 0)
pt4 = gmsh.model.occ.addPoint(-0.5, 0.7, 0)
pt10 = gmsh.model.occ.addPoint(0.6, -0.4, 0)
pt11 = gmsh.model.occ.addPoint(0.8, 0.5, 0)

line1 = gmsh.model.occ.addLine(pt1, pt2)
line2 = gmsh.model.occ.addLine(pt2, pt3)
line3 = gmsh.model.occ.addLine(pt3, pt4)
line4 = gmsh.model.occ.addLine(pt4, pt1)
line5 = gmsh.model.occ.addLine(pt10, pt11)
line6 = gmsh.model.occ.addLine(pt3, pt11)
line7 = gmsh.model.occ.addLine(pt10, pt2)
line8 = gmsh.model.occ.addLine(pt2, pt3)


rect1_loop = gmsh.model.occ.addCurveLoop([line1, line2, line3, line4])
rect2_loop = gmsh.model.occ.addCurveLoop([line8, line6, line5, line7])
rectOuter_loop = gmsh.model.occ.addCurveLoop([line1, line7, line5, line6, line3, line4]) # use for fragmenting, to avoid fragment merging the overlapping line2 and line8

rect1 = gmsh.model.occ.addSurfaceFilling(rect1_loop)
rect2 = gmsh.model.occ.addSurfaceFilling(rect2_loop)
rectOuter = gmsh.model.occ.addSurfaceFilling(rectOuter_loop)

bnd_cir = gmsh.model.occ.addCircle(0, 0, 0, 3)
gmsh.model.occ.synchronize()

circle_loop = gmsh.model.occ.addCurveLoop([bnd_cir])
circle = gmsh.model.occ.addSurfaceFilling(circle_loop)

gmsh.model.occ.fragment([(2, rectOuter)], [(2, circle)])

###
### set physical tags
###
# convert list of dimtags to tag
elem_tag_air = 4
elem_tag_right = 2
elem_tag_left = 1

# load physical tags
phys_tag_air = int(gmsh.parser.getNumber("phys_tag_air")[0])
phys_tag_left = int(gmsh.parser.getNumber("phys_tag_left")[0])
phys_tag_right = int(gmsh.parser.getNumber("phys_tag_right")[0])
phys_tag_interface_right = int(gmsh.parser.getNumber("phys_tag_interface_right")[0])
phys_tag_interface_left = int(gmsh.parser.getNumber("phys_tag_interface_left")[0])

gmsh.model.occ.remove([(2, rectOuter)])
gmsh.model.occ.synchronize()

gmsh.model.addPhysicalGroup(dim=2, tags=[elem_tag_left], tag=phys_tag_left)
gmsh.model.setPhysicalName(dim=2, tag=phys_tag_left, name="Coil_Left")
gmsh.model.addPhysicalGroup(dim=2, tags=[elem_tag_right], tag=phys_tag_right)
gmsh.model.setPhysicalName(dim=2, tag=phys_tag_right, name="Coil_Right")
gmsh.model.addPhysicalGroup(dim=2, tags=[elem_tag_air], tag=phys_tag_air)
gmsh.model.setPhysicalName(dim=2, tag=phys_tag_air, name="Air")
gmsh.model.addPhysicalGroup(dim=1, tags=[line2], tag=phys_tag_interface_left)
gmsh.model.setPhysicalName(dim=1, tag=phys_tag_interface_left, name="Coil_Interface_Left")
gmsh.model.addPhysicalGroup(dim=1, tags=[line8], tag=phys_tag_interface_right)
gmsh.model.setPhysicalName(dim=1, tag=phys_tag_interface_right, name="Coil_Interface_Right")


gmsh.option.setNumber("Mesh.MeshSizeMax", 0.2)
gmsh.option.setNumber("Mesh.Optimize", 1)

gmsh.model.mesh.set_transfinite_curve(line2, 8)
gmsh.model.mesh.set_transfinite_curve(line8, 19)

gmsh.model.mesh.generate(2)
# refine mesh on right side

###
### Cohomology
###

cuts_base_tag = 100
elem_tag_arbitrary_point = 1
gmsh.model.addPhysicalGroup(dim=0, tags=[elem_tag_arbitrary_point], tag=cuts_base_tag)
gmsh.model.setPhysicalName(dim=0, tag=cuts_base_tag, name="Dummy Region")

gmsh.model.mesh.addHomologyRequest(type="Cohomology", domainTags=[phys_tag_air], subdomainTags=[], dims=[1])
cuts = gmsh.model.mesh.computeHomology()

# rename cuts
for i in range(len(cuts)):
    cut_dimtag = cuts[i]
    dim = cut_dimtag[0]
    tag = cut_dimtag[1]
    old_name = gmsh.model.getPhysicalName(dim, tag)
    gmsh.model.removePhysicalName(old_name)
    gmsh.model.setPhysicalName(dim=dim, tag=cuts_base_tag+i+1, name=("Cut_"+str(i+1)))

###
### Save and show results
###

gmsh.write("mag.msh")
gmsh.write("mag.brep")

if '-nopopup' not in sys.argv:
    gmsh.fltk.run()  # open gui

gmsh.finalize()