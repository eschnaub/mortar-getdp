Include "general_parameters.pro";
Include "geometry_parameters.pro";

Group {
    Coil_Left = Region[phys_tag_left];
    Coil_Right = Region[phys_tag_right];
    Coil = Region[{Coil_Left, Coil_Right}];

    Air = Region[phys_tag_air];

    dom_H_1 = Region[{Coil_Left}];
    dom_H_2 = Region[{Coil_Right}];
    dom_H = Region[{dom_H_1, dom_H_2}];
    dom_Phi_1 = Region[{}];
    dom_Phi_2 = Region[{}];
    dom_Phi = Region[Air];

    Left = Region[{dom_H_1, dom_Phi_1}];
    Right = Region[{dom_H_2, dom_Phi_2}];

    n_cuts = 1;
    Cut = Region[{}];
    For i In {1 : n_cuts}
        idx = 100+i;
        Cut~{i} = Region[idx];
        Cut = Region[{Cut, Cut~{i}}];
    EndFor

    Shell_Minus = Region[phys_tag_interface_left];
    Shell_Plus = Region[phys_tag_interface_right];
}

Function {
    mu_0 = 4*Pi*1e-7;
    mu[] = 1. * mu_0;
    I[] = 50.;
    sigma[] = 1e5;
}


Constraint {
    { Name Current;
        Case {
            { Region Cut; Type Assign; Value I[]; TimeFunction $Time; }
        }
    }
    { Name Voltage;
        Case {
        }
    }
}

FunctionSpace {
    { Name Hcurl; Type Form1;
        BasisFunction {
            { Name sn1; NameOfCoef hn1; Function BF_Edge;
            Support Region[{dom_H~{1}, Shell_Minus}]; Entity EdgesOf[ All, Not {dom_Phi} ]; }
            { Name vn; NameOfCoef phi_n; Function BF_GradNode;
            Support Region[{dom_Phi, dom_H, Shell_Plus, Shell_Minus}]; Entity NodesOf[ dom_Phi ]; }
            { Name ci; NameOfCoef Ii; Function BF_GroupOfEdges;
            Support Region[{dom_H, dom_Phi}]; Entity GroupsOfEdgesOf[Cut]; }
            { Name sn2; NameOfCoef hn2; Function BF_Edge;
            Support Region[{dom_H~{2}, Shell_Plus}]; Entity EdgesOf[ All, Not {dom_Phi} ]; }
        }
        GlobalQuantity {
            { Name I; Type AliasOf; NameOfCoef Ii; }
            { Name V; Type AssociatedWith; NameOfCoef Ii; }
        }
        Constraint {
            { NameOfCoef I; EntityType GroupsOfEdgesOf; NameOfConstraint Current; }
            { NameOfCoef V; EntityType GroupsOfEdgesOf; NameOfConstraint Voltage; }
        }

        SubSpace {
            { Name Left_Coil; NameOfBasisFunction {sn1, ci, vn};}
            { Name Right_Coil; NameOfBasisFunction {sn2, ci, vn};}
        }
    }
    { Name Hcurl_lambda; Type Form1;
        BasisFunction {
            { Name sn; NameOfCoef vn; Function BF_Edge;
            Support Shell_Plus; Entity EdgesOf [ All, Not dom_Phi ]; }

            { Name sn_grad; NameOfCoef vn_grad; Function BF_GradNode;
                Support Shell_Plus; Entity EdgesOf [ dom_Phi ]; }
        }
    }
}

Jacobian {
    { Name Vol;
    Case {
        { Region All; Jacobian Vol;}
    }
    }
    { Name Sur;
    Case {
        { Region All; Jacobian Sur;}
    }
    }
}

Integration {
    { Name Int;
    Case {
        { Type Gauss;
            Case {
              { GeoElement Point;        NumberOfPoints  1; }
              { GeoElement Line;         NumberOfPoints  4; }
              { GeoElement Triangle;     NumberOfPoints  3; }
              { GeoElement Quadrangle;   NumberOfPoints  4; }
              { GeoElement Tetrahedron;  NumberOfPoints  5; }
              { GeoElement Hexahedron;   NumberOfPoints  6; }
              { GeoElement Prism;        NumberOfPoints  5; }
              { GeoElement Pyramid;      NumberOfPoints  8; }
            }
        }
    }
    }
}

Formulation{
    { Name mag; Type FemEquation;
      Quantity {
          { Name h; Type Local; NameOfSpace Hcurl; }
          { Name h_1; Type Local; NameOfSpace Hcurl[Left_Coil]; }
          { Name h_2; Type Local; NameOfSpace Hcurl[Right_Coil]; }
          { Name lambda; Type Local; NameOfSpace Hcurl_lambda; }
          { Name I; Type Global; NameOfSpace Hcurl[I]; }
          { Name V; Type Global; NameOfSpace Hcurl[V]; }
      }
      Equation {
          // PDE
          Integral{[1/sigma[] * Dof{d h}, {d h}];
          In dom_H; Jacobian Vol; Integration Int; }
          Integral{DtDof[ mu[] * Dof{h}, {h}];
          In Region[{dom_H, dom_Phi}]; Jacobian Vol; Integration Int; }
          // excitation
          GlobalTerm{[Dof{V}, {I}]; In Cut; }
          // mortar
          Integral{[ Trace[Dof{lambda}, Shell_Plus], {h_1}];
          In Shell_Minus; Jacobian Sur; Integration Int; }
          Integral{[ -Dof{lambda}, {h_2}];
          In Shell_Plus; Jacobian Sur; Integration Int; }

          Integral{[ Trace[Dof{h_1}, Shell_Minus], {lambda}];
          In Shell_Plus; Jacobian Sur; Integration Int; }
          Integral{[-Dof{h_2}, {lambda}];
          In Shell_Plus; Jacobian Sur; Integration Int; }


      }
    }
}

Resolution {
    { Name res;
        System {
            { Name sys_mag; NameOfFormulation mag; NameOfMesh "mag.msh"; }
        }
        Operation {
            InitSolution[sys_mag];

            TimeLoopTheta[t_0, t_end, t_step, t_theta] {
                Generate[sys_mag];
                Solve[sys_mag];
            }
            SaveSolution[sys_mag];
        }
    }
}

PostProcessing {
    { Name mag; NameOfFormulation mag;
    Quantity {
        { Name h; Value {
            Local{[ {h}]; In Region[{dom_H, dom_Phi}]; Jacobian Vol; }
        }}

        { Name b; Value {
            Local{[ mu[] * {h} ]; In Region[{dom_H, dom_Phi}]; Jacobian Vol; }
        }}
        { Name j; Value {
            Local{[ {d h} ]; In Region[{dom_H, dom_Phi}]; Jacobian Vol; }
        }}
    }
    }
}

PostOperation {
    { Name map; NameOfPostProcessing mag;
        Operation {
            Print[ h, OnElementsOf Region[{dom_Phi, dom_H}], File "h.pos"];
            Print[ b, OnElementsOf Region[{dom_Phi, dom_H}], File "b.pos"];
            Print[ j, OnElementsOf Region[{dom_H}], File "j.pos"];
        }
    }
}