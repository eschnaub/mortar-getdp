This is a small collection of .pro files for GetDP using mortar methods dealing with non-conforming meshes. 

They are related to [GetDP issue 109](https://gitlab.onelab.info/getdp/getdp/-/issues/109) and require GetDP version >= 3.5.0 to run.

Further related .pro files are found in the [Mortar Thin Shell Approximation public repository](https://git.rwth-aachen.de/roberthansluca.hahn/mortar-tsa).
