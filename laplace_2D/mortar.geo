SetFactory("OpenCASCADE");

DefineConstant[L1 = {0.3, Min 0.01, Max 0.99, Step 0.01, Name "Dimensions/L1"}];
DefineConstant[L2 = {0.7, Min 0.01, Max 0.99, Step 0.01, Name "Dimensions/L2"}];
DefineConstant[cl1 = {0.13, Name "Dimensions/Size 1"}];
DefineConstant[cl2 = {0.22, Name "Dimensions/Size 2"}];
Point(1) = {0,0,0,cl1};
Point(2) = {L1,0,0,cl1};
Point(3) = {L2,1,0,cl1};
Point(4) = {0,1,0,cl1};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Curve Loop(1) = {1:4};
Plane Surface(1) = {1};

Point(5) = {L1,0,0,cl2};
Point(6) = {1,0,0,cl2};
Point(7) = {1,1,0,cl2};
Point(8) = {L2,1,0,cl2};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,5};
Curve Loop(2) = {5:8};
Plane Surface(2) = {2};

Physical Surface("LEFT", 1) = 1;
Physical Surface("RIGHT", 2) = 2;
Physical Line("A",11) = 4;
Physical Line("B",12) = 2;
Physical Line("C",13) = 8;
Physical Line("D",14) = 6;
