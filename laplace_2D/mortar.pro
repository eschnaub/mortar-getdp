Group {

  Left = Region[1];
  Right = Region[2];
  A = Region[11];
  B = Region[12];
  C = Region[13]; // ovelaps B
  D = Region[14];

  Vol_Ele = Region[ {Left,Right} ];
}

Function {
  epsilon[] = 1.;
}

Constraint {
  { Name Dirichlet_Ele; Type Assign;
    Case {
      { Region A; Value 0.; }
      { Region D; Value 1; }
    }
  }
}

FunctionSpace {
  { Name H_v1; Type Form0;
    BasisFunction {
      { Name sn; NameOfCoef vn; Function BF_Node;
        Support Region[{Left, B}]; Entity NodesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef vn; EntityType NodesOf; NameOfConstraint Dirichlet_Ele; }
    }
  }
  { Name H_v2; Type Form0;
    BasisFunction {
      { Name sn; NameOfCoef vn; Function BF_Node;
        Support Region[{Right, C}]; Entity NodesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef vn; EntityType NodesOf; NameOfConstraint Dirichlet_Ele; }
    }
  }
  { Name H_lambda; Type Form0;
    BasisFunction{
      { Name si; NameOfCoef li; Function BF_Node;
	Support Region[ {C} ]; Entity NodesOf[All]; }
    }
  }
}

Jacobian {
  { Name Vol ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name Int ;
    Case { { Type Gauss ;
             Case { { GeoElement Line        ; NumberOfPoints  4 ; }
                    { GeoElement Triangle    ; NumberOfPoints  4 ; }
                    { GeoElement Quadrangle  ; NumberOfPoints  4 ; } }
      }
    }
  }
}

Formulation {
  { Name Electrostatics_v; Type FemEquation;
    Quantity {
      { Name v1; Type Local; NameOfSpace H_v1; }
      { Name v2; Type Local; NameOfSpace H_v2; }
      { Name lambda; Type Local ; NameOfSpace H_lambda; }
    }
    Equation {
      Integral { [ epsilon[] * Dof{d v1} , {d v1} ];
        In Left; Jacobian Vol; Integration Int; }
      Integral { [ epsilon[] * Dof{d v2} , {d v2} ];
        In Right; Jacobian Vol; Integration Int; }
      Integral { [ Trace[ Dof{lambda} , C ] , {v1} ];
        In B; Jacobian Sur; Integration Int; }

      Integral { [ - Dof{lambda} , {v2} ];
        In C; Jacobian Sur; Integration Int; }
      Integral { [ Trace[ Dof{v1} , B ] , {lambda} ] ;
        In C; Jacobian Sur ; Integration Int ; }
      Integral { [ - Dof{v2} , {lambda} ] ;
        In C; Jacobian Sur ; Integration Int ; }
    }
  }
}

Resolution {
  { Name EleSta_v;
    System {
      { Name Sys_Ele; NameOfFormulation Electrostatics_v; }
    }
    Operation {
      Generate[Sys_Ele]; Solve[Sys_Ele]; SaveSolution[Sys_Ele];
    }
  }
}

PostProcessing {
  { Name EleSta_v; NameOfFormulation Electrostatics_v;
    Quantity {
      { Name v; Value {
          Local { [ {v1} ]; In Left; Jacobian Vol; }
          Local { [ {v2} ]; In Right; Jacobian Vol; }

        }
      }
    }
  }
}

PostOperation {
  { Name Map; NameOfPostProcessing EleSta_v;
     Operation {
       Print [ v, OnElementsOf Vol_Ele, File "v.pos" ];
     }
  }
}
