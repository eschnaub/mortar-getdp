# Contents 

Three implementations of a very simple 3D H-phi problem. 
- `classical_H-phi`: standard H-phi with conforming meshes; to be seen as reference
- `linked_H-phi`: standard H-phi with the mesh split in two separate halves; the intersection of the two halves is conforming; in the function space the DoF are equated in a strong sense using a `Link` constraint
- `mortar_H-phi`: mortar H-phi implementation which was used for the findings as found below

# Trace operator for 3D edge elements

The 3D impementation of the `Trace` operator in GetDP is not robust for 3D edge elements. There are several symptoms that have been observed 

## Order of the basis functions matters for the Trace computation 

The order in which the basis functions inside a function space are defined matters for the correctness of a formulation which uses the `Trace` operator. This is seen when changing the support of the Lagrange multiplier (e.g., from Shell_Plus to Shell_Minus). This can be tested even if the two sides are actually conformally connected and thus it should not matter where the Lagrange multiplier is supported.

If the Trace operator is used (e.g., within a subspace), these two are not identical 

```
              { Name sn1; NameOfCoef hn1; Function BF_Edge;
                  Support Region[{dom_H~{1}, Shell_Minus}]; Entity EdgesOf[ All, Not {dom_Phi} ]; }
              { Name vn; NameOfCoef phi_n; Function BF_GradNode;
              Support Region[{dom_Phi, dom_H}]; Entity NodesOf[ dom_Phi ]; }
              { Name ci; NameOfCoef Ii; Function BF_GroupOfEdges;
              Support Region[{dom_H, dom_Phi}]; Entity GroupsOfEdgesOf[Cut]; }
              { Name sn2; NameOfCoef hn2; Function BF_Edge;
                  Support Region[{dom_H~{2}, Shell_Plus}]; Entity 
 EdgesOf[ All, Not {dom_Phi} ]; }
```
vs. 
```
              { Name sn2; NameOfCoef hn2; Function BF_Edge;
                  Support Region[{dom_H~{2}, Shell_Plus}]; Entity EdgesOf[ All, Not {dom_Phi} ]; }
              { Name sn1; NameOfCoef hn1; Function BF_Edge;
                  Support Region[{dom_H~{1}, Shell_Minus}]; Entity EdgesOf[ All, Not {dom_Phi} ]; }
              { Name vn; NameOfCoef phi_n; Function BF_GradNode;
              Support Region[{dom_Phi, dom_H}]; Entity NodesOf[ dom_Phi ]; }
              { Name ci; NameOfCoef Ii; Function BF_GroupOfEdges;
              Support Region[{dom_H, dom_Phi}]; Entity 
 GroupsOfEdgesOf[Cut]; }
```
 
Depending on where the Lagrange multiplier is defined, one of the above will lead to sensible results while the other will not.

### Order also matters for different function spaces

To circumvent the above, one might now try to split the above contributions into different function spaces. In this case, the order of the definition of the function space matters. Hence, this is not a suitable workaround.

# Triangular interfaces

Also, triangular interfaces do not seem to work, even if structured. This is true for non-conforming meshes only, for conforming (but identical) it works as expected.