import gmsh 

gmsh.initialize()
gmsh.model.add("t1")

gmsh.option.setNumber("Mesh.Algorithm", 6)

gmsh.option.setNumber("Mesh.Algorithm3D", 1)
len_z = 2

widths = [[1.5, 1.5],[0.5, 2, 0.5], [0.5, 2, 0.5] , [0.5, 2, 0.5],[3]]
heights = [1, 0.5, 0.025, 0.25, 1]	
boxes = []

accumulated_height = 0
for idx, (row_widths, height) in enumerate(zip(widths, heights)):
    accumulated_width = 0
    boxes.append([])
    for width in row_widths:
        boxes[idx].append(gmsh.model.occ.add_box(accumulated_width, accumulated_height, 0, width, height, len_z))
        accumulated_width += width
    accumulated_height += height

box_object = [(3, box) for box in boxes[0]]
box_tool = [(3, box) for box_list in boxes[1:] for box in box_list]

gmsh.model.occ.fragment(box_object, box_tool)

gmsh.model.occ.synchronize()

air_tags = boxes[0] + [boxes[1][0], boxes[2][0], boxes[1][2], boxes[2][2], boxes[3][0], boxes[3][2]] + boxes[4]
coil_tags = [boxes[1][1], boxes[2][1], boxes[3][1]]

left_bnd = gmsh.model.get_boundary([(3, boxes[0][0])], oriented=False)
right_bnd = gmsh.model.get_boundary([(3, boxes[0][1])], oriented=False)

cut_tag = gmsh.model.occ.intersect(left_bnd, right_bnd)[0][0][1]

shell_tag_left = 33
shell_tag_right = 86

gmsh.model.occ.translate([(3, box) for box_list in boxes[3:] for box in box_list], 0, 1, 0)
gmsh.model.occ.translate([(3, box) for box_list in boxes[3:] for box in box_list], 0, -1, 0)	
gmsh.model.occ.synchronize()

air_interfaces_left = [29, 37]
air_interfaces_right = [81, 91]

translation = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1]

gmsh.model.mesh.set_periodic(2, air_interfaces_left, air_interfaces_right, translation)
gmsh.model.mesh.set_periodic(2, [shell_tag_left], [shell_tag_right], translation)
num_nodes=25
gmsh.model.mesh.set_transfinite_curve(49, num_nodes)
gmsh.model.mesh.set_transfinite_curve(54, num_nodes)
gmsh.model.mesh.set_transfinite_curve(56, num_nodes)
gmsh.model.mesh.set_transfinite_curve(57, num_nodes)

gmsh.model.mesh.set_transfinite_surface(shell_tag_left)
gmsh.model.mesh.set_transfinite_surface(shell_tag_right)

# this is needed for a nice result
#gmsh.model.mesh.set_recombine(2, shell_tag_left)
#gmsh.model.mesh.set_recombine(2, shell_tag_right)

gmsh.model.add_physical_group(dim=2, tags=[cut_tag], tag=1, name="Cut")
gmsh.model.add_physical_group(dim=3, tags=air_tags, tag=2, name="Air")
gmsh.model.add_physical_group(dim=3, tags=[coil_tags[1], ], tag=10, name="Coil Left")
gmsh.model.add_physical_group(dim=3, tags=[coil_tags[0]], tag=3, name="Coil Left")
gmsh.model.add_physical_group(dim=3, tags=[coil_tags[2]], tag=4, name="Coil Right")
gmsh.model.add_physical_group(dim=2, tags=[shell_tag_left], tag=5, name="Shell Left")
gmsh.model.add_physical_group(dim=2, tags=[shell_tag_right], tag=6, name="Shell Right")
gmsh.model.add_physical_group(dim=2, tags=air_interfaces_left, tag=8, name="Air Interface Left")
gmsh.model.add_physical_group(dim=2, tags=air_interfaces_right, tag=9, name="Air Interface Right")

gmsh.option.setNumber("Mesh.MeshSizeMax", 1E-1)

#
# this workaround is used to still be able to use the cohomolgy plugin 
# to generate the edge elements with support of one element layer 
# on one side of the cut 
# we could change the formulation to avoid using the plugin
gmsh.model.add_physical_group(dim=3, tags=[7], tag=7, name="Helper for Cut")

gmsh.model.mesh.generate(3)
gmsh.model.mesh.addHomologyRequest(type="Cohomology", domainTags=[2, 7], subdomainTags=[1], dims=[1])
cuts = gmsh.model.mesh.computeHomology()

gmsh.write("test_geo_link.msh")

gmsh.fltk.run()
gmsh.finalize()