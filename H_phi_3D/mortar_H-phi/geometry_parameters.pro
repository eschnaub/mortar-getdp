// size of domain
size_x = 1.;
size_y = 1.;
size_z = 1.;

// size of conductor
w_x = 0.2;
w_y = 0.2;
w_z = 0.7;
d_z = 0.2;

// physical tags to assign
phys_tag_air = 2;
phys_tag_left = 3;
phys_tag_right = 4;
phys_tag_interface_right = 6;
phys_tag_interface_left = 5;
