Include "general_parameters.pro";

Group {

    Coil = Region[3];

    Air = Region[2];

    dom_H = Region[Coil];

    dom_Phi = Region[Air];

    Cut = Region[4];
    
}

Function {
    mu_0 = 4*Pi*1e-7;
    mu[] = 1. * mu_0;
    I[] = 50.;
    sigma[] = 1e5;
}


Constraint {
    { Name Current;
        Case {
            { Region Cut; Type Assign; Value I[]; TimeFunction $Time; }
        }
    }
    { Name Voltage;
        Case {
        }
    }
}

FunctionSpace {
    { Name Hcurl; Type Form1;
        BasisFunction {
            { Name sn; NameOfCoef hn; Function BF_Edge;
                Support dom_H; Entity EdgesOf[ All, Not {dom_Phi} ]; }
            { Name vn; NameOfCoef phin; Function BF_GradNode;
            Support Region[{dom_H, dom_Phi}]; Entity NodesOf[ dom_Phi ]; }
            { Name ci; NameOfCoef Ii; Function BF_GroupOfEdges;
            Support Region[{dom_H, dom_Phi}]; Entity GroupsOfEdgesOf[Cut]; }
        }
        GlobalQuantity {
            { Name I; Type AliasOf; NameOfCoef Ii; }
            { Name V; Type AssociatedWith; NameOfCoef Ii; }
        }
        Constraint {
            { NameOfCoef I; EntityType GroupsOfEdgesOf; NameOfConstraint Current; }
            { NameOfCoef V; EntityType GroupsOfEdgesOf; NameOfConstraint Voltage; }
        }
    }
}

Jacobian {
    { Name Vol;
    Case {
        { Region All; Jacobian Vol;}
    }
    }
    { Name Sur;
    Case {
        { Region All; Jacobian Sur;}
    }
    }
}

Integration {
    { Name Int;
    Case {
        { Type Gauss;
            Case {
              { GeoElement Point;        NumberOfPoints  1; }
              { GeoElement Line;         NumberOfPoints  4; }
              { GeoElement Triangle;     NumberOfPoints  3; }
              { GeoElement Quadrangle;   NumberOfPoints  4; }
              { GeoElement Tetrahedron;  NumberOfPoints  5; }
              { GeoElement Hexahedron;   NumberOfPoints  6; }
              { GeoElement Prism;        NumberOfPoints  5; }
              { GeoElement Pyramid;      NumberOfPoints  8; }
            }
        }
    }
    }
}

Formulation{
    { Name mag; Type FemEquation;
      Quantity {
          { Name h; Type Local; NameOfSpace Hcurl; }
       
          { Name I; Type Global; NameOfSpace Hcurl[I]; }
          { Name V; Type Global; NameOfSpace Hcurl[V]; }

      }
      Equation {
          // non-cross-terms
          Integral{[1/sigma[] * Dof{d h}, {d h}];
          In dom_H; Jacobian Vol; Integration Int; }

          Integral{DtDof[ mu[] * Dof{h}, {h}];
          In Region[{dom_H, dom_Phi}]; Jacobian Vol; Integration Int; }
      
          // excitation
          GlobalTerm{[Dof{V}, {I}]; In Cut; }
          // mortar

      }
    }
}

Resolution {
    { Name res;
        System {
            { Name sys_mag; NameOfFormulation mag; NameOfMesh "test_geo.msh"; }
        }
        Operation {
            InitSolution[sys_mag];

            TimeLoopTheta[t_0, t_end, t_step, t_theta] {
                Generate[sys_mag];
                Solve[sys_mag];
            }
            SaveSolution[sys_mag];
        }
    }
}

PostProcessing {
    { Name mag; NameOfFormulation mag;
    Quantity {
        { Name h; Value {
            Local{[ {h}]; In Region[{dom_H, dom_Phi}]; Jacobian Vol; }
        }}


        { Name j; Value {
            Local{[ {d h}]; In Region[{dom_H, dom_Phi}]; Jacobian Vol; }
        }}
    }
    }
}

PostOperation {
    { Name map; NameOfPostProcessing mag;
        Operation {
            Print[ h, OnElementsOf Region[{dom_Phi, dom_H}], File "h.pos"];
            Print[ j, OnElementsOf Region[{dom_H}], File "j.pos"];
        }
    }
}
