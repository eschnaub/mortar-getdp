import gmsh 

gmsh.initialize()
gmsh.model.add("t1")

len_z = 2

widths = [[1.5, 1.5],[0.5, 2, 0.5], [0.5, 2, 0.5],[3]]
heights = [1, 0.5, 0.5, 1]	
boxes = []

accumulated_height = 0
for idx, (row_widths, height) in enumerate(zip(widths, heights)):
    accumulated_width = 0
    boxes.append([])
    for width in row_widths:
        boxes[idx].append(gmsh.model.occ.add_box(accumulated_width, accumulated_height, 0, width, height, len_z))
        accumulated_width += width
    accumulated_height += height

box_object = [(3, box) for box in boxes[0]]
box_tool = [(3, box) for box_list in boxes[1:] for box in box_list]

gmsh.model.occ.fragment(box_object, box_tool)

gmsh.model.occ.synchronize()

left_bnd = gmsh.model.get_boundary([(3, boxes[0][0])], oriented=False)
right_bnd = gmsh.model.get_boundary([(3, boxes[0][1])], oriented=False)

cut_tag = gmsh.model.occ.intersect(left_bnd, right_bnd)[0][0][1]

air_tags = boxes[0] + boxes[3] + [boxes[1][0], boxes[2][0], boxes[1][2], boxes[2][2]]
coil_tags = [boxes[1][1], boxes[2][1]]

gmsh.model.add_physical_group(dim=2, tags=[cut_tag], tag=1, name="Cut")
gmsh.model.add_physical_group(dim=3, tags=air_tags, tag=2, name="Air")
gmsh.model.add_physical_group(dim=3, tags=coil_tags, tag=3, name="Coil")
gmsh.option.setNumber("Mesh.MeshSizeMax", 1E-1)

gmsh.model.mesh.generate(3)

gmsh.model.mesh.addHomologyRequest(type="Cohomology", domainTags=[2], subdomainTags=[1], dims=[1])
cuts = gmsh.model.mesh.computeHomology()

gmsh.write("test_geo.msh")

gmsh.fltk.run()
gmsh.finalize()